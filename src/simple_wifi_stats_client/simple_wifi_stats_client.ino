#include "WiFi.h"
#include <odroid_go.h>

const char* ssid = "REDACTED";
const char* password = "REDACTED";

WiFiClient statsClient;
IPAddress statsServerAddress(192,168,1,1);

void setup() {

  GO.begin();
 
  WiFi.begin(ssid, password);

  GO.lcd.print("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    GO.lcd.print(".");
  }

  GO.lcd.println();
  GO.lcd.println("Connected!");
  GO.lcd.println(WiFi.localIP());

  statsClient.connect(statsServerAddress, 2003);
 
}

void loop() {
  if (statsClient.connected() ) {
    statsClient.print("some.stat ");
    statsClient.print(random(300));
    statsClient.println(" -1");
  }

  delay(1000);
}
