The latest and greatest version of this document can be found at [https://gitlab.com/pithy.tech/kernelcon_handouts](https://gitlab.com/pithy.tech/kernelcon_handouts/)
# IDE Setup #

1.  Download and install the latest / greatest (as of this writing, 1.8.9) desktop flavor of the Arduino IDE from [https://www.arduino.cc/en/Main/Software](https://www.arduino.cc/en/Main/Software)  Note:  If on Windows, do _not_ use the one from the app store, it is a bit weird.
2.  Launch the Arduino IDE

## Add Board URLs To Arduino ##

1.  In the Preferences screen (Arduino -> Preferences if on MacOS, File -> Preferences elsewhere) add this string to the Additional Boards Manager URLs: `https://dl.espressif.com/dl/package_esp32_index.json`
2.  Save changes and close the Preferences screen

## Install Board Support ##  

1.  Click on Tools -> Board [Blah Blah] -> Boards Manager
2.  In the Boards Manager dialog, type "esp32" in the filter field.
3.  Select "esp32 by Espressif Systems" and click "Install"
4.  Wait for the magic to occur
5.  Close the Boards Manager dialog.

## Verify Installation

1.  Select the "ODROID ESP32" board from the Tools -> Board menu.
2.  Click the "Verify" button in the Arduino IDE.  (This is the one that looks like a check mark)
3.  Wait while the Arduino IDE uses your newly installed ESP32 compiler to compile your current sketch.
4.  When you get the message "Done Compiling" you are now ready to compile programs for your ODROID-GO!

## Install ODROID-GO "Special Sauce" ##

1.  Open a terminal window of some ilk.
2.  Use git to pull down the latest / greatest ODROID-GO libraries

On Linux
`git clone https://github.com/hardkernel/ODROID-GO.git ~/Arduino/libraries/ODROID-GO`

On MacOS
`git clone https://github.com/hardkernel/ODROID-GO.git ~/Documents/Arduino/libraries/ODROID-GO`

On Windows
`git clone https://github.com/hardkernel/ODROID-GO.git %USERPROFILE%/Documents/Arduino/libraries/ODROID-GO`

3.  Restart the Arduino IDE so it has a chance to pick up these new goodies

## Try It All Out ###

1.  In the Arduino IDE, go to File -> Examples, scroll waay down to the end and you should see "ODROID-GO" listed under the "Examples from Custom Libraries" heading
2.  Select an example (Let's grab Applications / FlappyBird, everyone loves FlappyBird) and you'll see its code load up in the IDE
3.  Plug your ODROID-GO into your PC using the included USB cable and turn it on.
4.  In the Tools menu, ensure that "ODROID ESP32" listed as your current board
5.  In the Tools -> Port menu, select the port that your ODROID has connected to.

In Windows this will be a COM port.

In Linux this should be /dev/ttyUSBn.

In MacOS you should see the delicate, artsy, and poetically named port /dev/cu.SLAB_USBtoUART.

**If you do not see these ports, please get the attention of the instructor.  You may be missing some drivers from [https://www.silabs.com](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)**

6.  Click on the "Upload" button in the Arduino IDE (it looks like a big right arrow).  You will see the program compile.  After a short time the ODROID-GO will reset and you'll be FlappyBirding like its 2013!
