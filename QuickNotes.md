## ODROID-GO Expansion Header(P2) Description ##

This table stolen shamelessly from [ODROID](https://wiki.odroid.com/odroid_go/odroid_go#odroid-go_header_p2_description) and modified

|Pin #|GPIO|Type|Function|i2c|
|-----|----|----|--------|---|
|1|GND|POWER|GND(0V)|GND|
|2|*1) VSPI.SCK(IO18)|OUT|GPIO18, VSPICLK||
|3|IO12|OUT|GPIO||
|4|IO15|IN/OUT|GPIO15, ADC2_CH3|SDA|
|5|IO4|IN/OUT|GPIO4, ADC2_CH0|SCL|
|6|P3V3|POWER|3.3 V|VCC|
|7|*1) VSPI.MISO(IO19)|IN/OUT|GPIO19, VSPIQ||
|8|*1) VSPI.MOSI(IO23)|OUT|GPIO23, VSPID||
|9|N.C|-|Not connect||
|10|*2) VBUS|POWER|USB VBUS (5V)||

*1) This SPI is shared with the ODROID-GO's 2.4Inch LCD and microSD slot.

*2) It can be used only when external USB power is connected.

## ODROID-GO LCD Reference ##
[Display.h](https://github.com/hardkernel/ODROID-GO/blob/master/src/utility/Display.h)

## Muting The Chirpy Speaker ##
```
pinMode(25, OUTPUT);
digitalWrite(25, LOW);
```

## Wire Library With ODROID GO Special Sauce ##

Use the sensors/Wire.h header to make sure everyone gets along.

```
#include <odroid_go.h>
#include "sensors/Wire.h"
```

## LM75 Temperature Sensor ##
[https://electricdollarstore.com/temp.html](https://electricdollarstore.com/temp.html)

## i2c LED Display ##
[https://electricdollarstore.com/dig2.html](https://electricdollarstore.com/dig2.html)

## Graphite TCP Plan-Text Protocol ##

IP Address: `FILL_THIS_IN_AT_WORKSHOP`

Port: 2003

```
name.of.metric <VALUE> <TIMESTAMP>(-1)
```

We're using -1 for timestamp as the ODROID-GOs themselves are not aware of the current time and we can use the timestamp on the server when recording data.

