## What Next? ##

*  [ODROID's Wiki - https://wiki.odroid.com/odroid_go/odroid_go](https://wiki.odroid.com/odroid_go/odroid_go)
*  Try MicroPython
*  Install the game emulator, get some ROMs, lose some time!
*  [Try some ported games - https://github.com/jkirsons](https://github.com/jkirsons)
*  Check out the Arduino Extension for VSCode
*  Try some cloud services like Adafruit.IO

## More Parts, More Fun! ##
*  [Adafruit - https://www.adafruit.com](https://www.adafruit.com)
*  [Electric Dollar Store - https://electricdollarstore.com/](https://electricdollarstore.com)
*  [Addicore - https://www.addicore.com](https://www.addicore.com)
*  [MPJA - https://www.mpja.com](https://www.mpja.com)
*  [Tindie - https://www.tindie.com](https://www.tindie.com)

^ The presenter(s) are not affiliated with any of these suppliers.  We're just big fans and happy customers!

## Demmed Intradasting ##
*  [The Prepared - https://theprepared.org/](https://theprepared.org/)

## Bug The Nice Fellows @ Pithy.Tech ##
*  E-Mail [info@pithy.tech](info@pithy.tech) to get on our exceedingly low-volume mailing list or ask us to cater your next holiday gathering!

