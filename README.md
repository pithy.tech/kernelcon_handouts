# kernelcon_handouts

Handouts for the Microcontroller Multitool 1/2 Day Workshop @ KernelCon 2019!

## WIFI Password: omastandard ##

## https://gitlab.com/pithy.tech/kernelcon_handouts ##

[Syllabus](Syllabus.md)

[Arduino IDE Setup](arduino_odroid-go_setup.md)

[Quick Notes!](QuickNotes.md)

[What Next?](what_next.md)
